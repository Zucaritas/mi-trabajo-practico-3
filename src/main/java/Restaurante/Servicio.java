// se abre registra,la hora de entrada, la mesa, cantidad de comensales, toma los pedidos y se cierra con hora de cierre 
package Restaurante;
import java.util.ArrayList;
public class Servicio{

    private ArrayList<Pedido> listaPedidos;
    private String horaIngreso;
    private String horaCierre;
    private int mesa;
    private String mozo;
    private int comensales;

    public Servicio(String horaIngreso, int mesa, int comensales, String mozo, String horaCierre){
        this.horaIngreso = horaIngreso;
        this.mesa = mesa;
        this.comensales = comensales;
        this.mozo = mozo;
        this.horaCierre = horaCierre;
        this.listaPedidos = new ArrayList<>();
    }

    public void agregarPedido(String producto, double precio){
        Pedido nuevoPedido = new Pedido (producto, precio);
        this.listaPedidos.add(nuevoPedido);
    }
    public ArrayList<Pedido> getMostrarPedido(){
        return this.listaPedidos;
    }
    public String getHoraIngreso(){
        return horaIngreso;
    }
    public String getHoraCierre(){
        return horaCierre;
    }
    public int getMesa(){
        return mesa;
    }
    public String getMozo(){
        return mozo;
    }
    public int getComensales(){
        return comensales;
    }
}