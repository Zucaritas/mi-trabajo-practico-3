package Restaurante;
public class Tarjeta{
    
    private String nombre;
    private String apellido;
    private String numeroTarjeta;
    private String codigoSeguridadTarjeta;
    private double recargo;

    public Tarjeta(String nombre, String apellido, String numeroTarjeta, String codigoSeguridadTarjeta, double recargo){

        this.nombre = nombre;
        this.apellido = apellido;
        this.numeroTarjeta = numeroTarjeta;
        this.codigoSeguridadTarjeta = codigoSeguridadTarjeta;
        this.recargo = recargo;

    }

    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getNumeroTarjeta(){
        return numeroTarjeta;
    }
    public String getCodigoSeguridadTarjeta(){
        return codigoSeguridadTarjeta;
    }
    public double getRecargo(){
        return recargo;
    }
}