//llama y calcula todo lo referido a las otras clases
package Restaurante;
import java.util.ArrayList;

public class Administracion{

    ArrayList<Pedido> listaPedido;

    public double generarFactura(Servicio cliente,boolean pagoTarjeta, String nombreTitular, String apellidoTitular, String codigoTarjeta, String codigoSeguridad, double recargo){
        double totalPago = 0;
        listaPedido = cliente.getMostrarPedido();

        for(Pedido productosTotales : listaPedido){
            totalPago += productosTotales.getPrecio();
        }

        if(pagoTarjeta == true){
            Tarjeta nuevaTarjeta = new Tarjeta(nombreTitular, apellidoTitular, codigoTarjeta, codigoSeguridad, recargo);
            nuevaTarjeta.getRecargo();
            return((totalPago * nuevaTarjeta.getRecargo())/100);
        }else{
            return totalPago;
        }
    }

    public void mostrarInformacion(Servicio informacion){
        System.out.println("Mozo: " + informacion.getMozo() + " Mesa: " + informacion.getMesa());
        listaPedido = informacion.getMostrarPedido();
        
        for(Pedido mostrarPedidos : listaPedido){
            System.out.println("Producto: " + mostrarPedidos.getProducto() + "  Precio: " +mostrarPedidos.getPrecio());
        }
    }

    public Administracion(){       
        listaPedido = new ArrayList<>();
    }

}