//Clase principal
package Restaurante;

public class Principal{
    public static void main(String []args){

        boolean tarjeta;
        String titularTarjetaNombre = "Francisca";
        String titularTarjetaApellido = "Quintero";
        String codigoTarjeta = " 2345 6789 0000 0000";
        String codigoSeguridad = "123";
        double recargo = 10;

        Administracion Restaurante = new Administracion();
        Servicio nuevoCliente = new Servicio("19:30 hrs",27,3,"Exequiel Cisterna","21:45 hrs.");

        tarjeta = false;

        nuevoCliente.agregarPedido("Soda 4 Lts, Albear",80);
        nuevoCliente.agregarPedido("Hamburgesa Zapping combo Papas", 145);

        double totalPagar = Restaurante.generarFactura(nuevoCliente, tarjeta,titularTarjetaNombre, titularTarjetaApellido, codigoTarjeta, codigoSeguridad, recargo);
    

        Restaurante.mostrarInformacion(nuevoCliente);
        System.out.println("Total a Pagar: " + totalPagar);

    }


    public Principal(){

    }
}