package Restaurante;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class AdministracionTest{

    @Test
    public void generarFacturaSinTarjeta(){
        double pagoSinTarjeta = 0;
        Servicio clientePrueba = new Servicio("18 hrs", 4, 2, "Joaquin", "20 hrs");
       
        clientePrueba.agregarPedido("Producto ", 10.00); 
        clientePrueba.agregarPedido("Producto 2", 10.00);
        clientePrueba.agregarPedido("Producto 3", 10.00);

        ArrayList<Pedido> listaPrueba;
        listaPrueba = clientePrueba.getMostrarPedido();
       
        for (Pedido productosDePrueba : listaPrueba) {
            pagoSinTarjeta += productosDePrueba.getPrecio();
        }
        assertEquals(30.00, pagoSinTarjeta, 0);
        }
}